package example

import fr.esgi.al2.funprog.Model.{Coordonnees, Direction, Mouvement, Pelouse, Tondeuse}
import fr.esgi.al2.funprog.Service.TondeuseService
import org.scalatest.funsuite.AnyFunSuite

class TestCase extends AnyFunSuite {


  test("Deplacer tondeuse") {
    val tondesueService = new TondeuseService
    val pelouse = Pelouse(Coordonnees(5, 5))

    val tendeuse1 = Tondeuse(Coordonnees(1, 2), Direction.N)
    val commands1: Seq[Mouvement.Value] = Seq(Mouvement.G, Mouvement.A, Mouvement.G, Mouvement.A, Mouvement.G, Mouvement.A, Mouvement.G, Mouvement.A, Mouvement.A)
    val movedTondeuse1 = commands1.foldLeft(tendeuse1)((accumulator, command)  => tondesueService.moveTondeuse(accumulator, pelouse, command))
    assert(1 == movedTondeuse1.coordonnees.x)
    assert(3 == movedTondeuse1.coordonnees.y)
    assert(Direction.N == movedTondeuse1.direction)

    val tendeuse2 = Tondeuse(Coordonnees(3, 3), Direction.E)
    val commands2: Seq[Mouvement.Value] = Seq(Mouvement.A, Mouvement.A, Mouvement.D, Mouvement.A, Mouvement.A, Mouvement.D, Mouvement.A, Mouvement.D, Mouvement.D, Mouvement.A)
    val movedTondeuse2 = commands2.foldLeft(tendeuse2)((accumulator, command)  => tondesueService.moveTondeuse(accumulator, pelouse, command))
    assert(5 == movedTondeuse2.coordonnees.x)
    assert(1 == movedTondeuse2.coordonnees.y)
    assert(Direction.E == movedTondeuse2.direction)
  }

}

package fr.esgi.al2.funprog.Service

import fr.esgi.al2.funprog.Model.{
  Coordonnees,
  Direction,
  Mouvement,
  Pelouse,
  Tondeuse
}

class TondeuseService {
  def moveTondeuse(
      inputTondeuse: Tondeuse,
      pelouse: Pelouse,
      mouvement: Mouvement.Value
  ): Tondeuse = {
    mouvement match {
      case Mouvement.G =>
        inputTondeuse.copy(direction = gaucheDroite(inputTondeuse.direction)._1)
      case Mouvement.D =>
        inputTondeuse.copy(direction = gaucheDroite(inputTondeuse.direction)._2)
      case Mouvement.A => {
        val newCoor =
          calCoordonnees(inputTondeuse.coordonnees, inputTondeuse.direction)
        if (newCoor.x >= 0 && newCoor.y >= 0 && newCoor.x <= pelouse.coinDroit.x && newCoor.y <= pelouse.coinDroit.y)
          inputTondeuse.copy(coordonnees = newCoor)
        else inputTondeuse
      }
      case _ => inputTondeuse
    }
  }

  private def gaucheDroite(
      currentDirection: Direction.Value
  ): (Direction.Value, Direction.Value) = currentDirection match {
    case Direction.N => (Direction.W, Direction.E)
    case Direction.E => (Direction.N, Direction.S)
    case Direction.S => (Direction.E, Direction.W)
    case Direction.W => (Direction.S, Direction.N)
    case _           => (currentDirection, currentDirection)
  }

  private def calCoordonnees(
      currentCoordinates: Coordonnees,
      currentDirection: Direction.Value
  ): Coordonnees = currentDirection match {
    case Direction.N => currentCoordinates.copy(y = currentCoordinates.y + 1)
    case Direction.E => currentCoordinates.copy(x = currentCoordinates.x + 1)
    case Direction.S => currentCoordinates.copy(y = currentCoordinates.y - 1)
    case Direction.W => currentCoordinates.copy(x = currentCoordinates.x - 1)
    case _           => currentCoordinates
  }
}

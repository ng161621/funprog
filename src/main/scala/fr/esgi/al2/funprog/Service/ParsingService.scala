package fr.esgi.al2.funprog.Service

import better.files.File

import fr.esgi.al2.funprog.Model.{
  Coordonnees,
  Direction,
  Mouvement,
  Pelouse,
  Tondeuse
}

class ParsingService {
  def getPelous(file: File): Option[Pelouse] = {
    val input = file.lines.toList
    if (!input.isEmpty) {
      val coordinateArray = input(0).split(" ")
      if (coordinateArray.length != 2) None
      else
        Some(
          Pelouse(
            Coordonnees(coordinateArray(0).toInt, coordinateArray(1).toInt)
          )
        )
    } else None
  }

  def getTondeuses(file: File): List[Tondeuse] = {
    val input = file.lines.toList
    if (!input.isEmpty && input.length > 1) {
      val tondeuseLines = input.zipWithIndex
        .filter {
          case (_, index) => index != 0 && index % 2 != 0
        }
        .map(_._1)
      tondeuseLines
        .filter(_.split(" ").length == 3)
        .map(line => {
          val tondeuseParams = line.split(" ")
          Tondeuse(
            Coordonnees(tondeuseParams(0).toInt, tondeuseParams(1).toInt),
            Direction.withName(tondeuseParams(2))
          )
        })
    } else List.empty[Tondeuse]
  }

  def getCommandes(file: File): List[List[Mouvement.Value]] = {
    val input = file.lines.toList
    if (!input.isEmpty && input.length > 2) {
      val commandLines = input.zipWithIndex
        .filter {
          case (_, index) => index != 0 && index % 2 == 0
        }
        .map(_._1)
      commandLines.map(
        line => line.toList.map(cmd => Mouvement.withName(cmd.toString))
      )
    } else List.empty[List[Mouvement.Value]]
  }
}

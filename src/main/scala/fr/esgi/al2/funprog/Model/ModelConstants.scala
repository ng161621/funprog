package fr.esgi.al2.funprog

package object Model {

  object Mouvement extends Enumeration {
    val D, G, A = Value
  }

  object Direction extends Enumeration {
    val N, E, W, S = Value
  }
}

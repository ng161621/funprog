package fr.esgi.al2.funprog.Model

case class Tondeuse(coordonnees: Coordonnees, direction: Direction.Value) {
  def print() =
    this.coordonnees.x.toString + " " + this.coordonnees.y.toString + " " + this.direction.toString
}

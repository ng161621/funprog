package fr.esgi.al2.funprog

import better.files.File
import com.typesafe.config.{Config, ConfigFactory}
import fr.esgi.al2.funprog.Model.Tondeuse
import fr.esgi.al2.funprog.Service.{ParsingService, TondeuseService}

object Main extends App {
  val conf: Config = ConfigFactory.load()

  val instructionFile: String = conf.getString("appplication.input-file")

  val inputFile = File(instructionFile)

  val parsingService = new ParsingService()
  val pelouse = parsingService.getPelous(inputFile)
  pelouse match {
    case Some(pelouse) =>
      val tondeuses = parsingService.getTondeuses(inputFile)
      val commandes = parsingService.getCommandes(inputFile)
      tondeuses.zipWithIndex.foreach {
        case (tondeuse, index)
            if (commandes.length >= index + 1) => {
          val tondeuseService = new TondeuseService()
          val deplaceTondeuse = commandes(index).foldLeft(tondeuse)(
            (tond, instruction) =>
              tondeuseService.moveTondeuse(tond, pelouse, instruction)
          )
          helper.printPosition(deplaceTondeuse, index)
        }
        case (tondeuse, index) =>
          helper.printPosition(tondeuse, index)
      }
    case None => print("No pelouse coordiantes found")
      ()
  }
}

object helper {
  def printPosition(tondeuse: Tondeuse, index: Int) = {
    println(tondeuse.print())
    index.toInt
  }
}
